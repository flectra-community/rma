# Flectra Community / rma

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[rma_sale](rma_sale/) | 2.0.2.3.2| Sale Order - Return Merchandise Authorization (RMA)
[rma](rma/) | 2.0.3.3.0| Return Merchandise Authorization (RMA)
[website_rma](website_rma/) | 2.0.1.0.1| Return Merchandise Authorization (RMA)
[rma_sale_mrp](rma_sale_mrp/) | 2.0.1.1.0| Allow doing RMAs from MRP kits
[rma_delivery](rma_delivery/) | 2.0.1.0.0| Allow to choose a default delivery carrier for returns
[product_warranty](product_warranty/) | 2.0.1.0.1| Product Warranty


